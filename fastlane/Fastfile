# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  desc "Runs all the tests"
  lane :test do
    gradle(task: "test")
  end

  desc "Build bundle for debugging"
  lane :buildFdroidDebugBundle do
    gradle(task: "bundle", flavor: "completeFdroid", build_type: "Debug")
  end

  desc "Build bundle for release"
  lane :buildFdroidReleaseBundle do
    gradle(task: "bundle", flavor: "completeFdroid", build_type: "Release")
  end

  desc "Build apk for debugging"
  lane :buildFdroidDebugApk do
    gradle(task: "assemble", flavor: "completeFdroid", build_type: "Debug")
  end

  desc "Build apk for release"
  lane :buildFdroidReleaseApk do
    gradle(task: "assemble", flavor: "completeFdroid", build_type: "Release")
  end

  desc "Submit a new Internal Build to Play Store"
  lane :internal do
    upload_to_play_store(track: 'internal', aab: 'assembly/build/outputs/bundle/completeFdroidRelease/assembly-complete-fdroid-release.aab')
  end

  desc "Promote Internal to Alpha"
  lane :promote_internal_to_alpha do |options|
    upload_to_play_store(track: 'internal', track_promote_to: 'alpha', version_code: options[:version_code])
  end

  desc "Promote Alpha to Beta"
  lane :promote_alpha_to_beta do
    upload_to_play_store(track: 'alpha', track_promote_to: 'beta', version_code: options[:version_code])
  end

  desc "Promote Beta to Production"
  lane :promote_beta_to_production do
    upload_to_play_store(track: 'beta', track_promote_to: 'production', version_code: options[:version_code])
  end

  desc "Build debug and test APK for screenshots"
  lane :screenshots do
    capture_android_screenshots
    upload_to_play_store
  end

  desc "Deploy a new version to the Google Play"
  lane :deploy do
    gradle(task: "clean assembleCompleteRelease")
    upload_to_play_store
  end
end
